
angular.module('brushfire_bookPage', []) 
.config(function($sceDelegateProvider) {
    $sceDelegateProvider.resourceUrlWhitelist([
        'self',
        '*://www.youtube.com/**'
    ]);
});

angular.module('brushfire_bookPage').controller('PageCtrl', [
    '$scope', '$timeout','$http',
    function($scope , $timeout , $http){
         $scope.videosLoading = true; 
         console.log("start. ");
         $http.get('/books').then(function(res){
             console.log('GET video ,data', res);
             $scope.books = res.data;
             $scope.booksLoading = false; 
         });
        
    
         $scope.submitNewBook = function() {
           console.log("start submitNewBook.....");
            if ($scope.busySubmittingImage) {
                return;
            }       
            
            if (angular.isUndefined($scope.newBookTitle) || angular.isUndefined($scope.newAuthorTitle) ) {
                return;
            } 
             var _newBook = {
                title: $scope.newBookTitle, 
                author: $scope.newAuthorTitle,
                image: $scope.newImagSrc, 
            };
            if ( angular.isUndefined($scope.newImagSrc )) {
                _newBook.image = '/images/ImageBook/defBook.png';
            }
            
            var parser = document.createElement('a');
        //     parser.href = _newBook.src
        //     var youtubeID = parser.search.substring(parser.search.indexOf("=") + 1,  parser.search.length);
        //   /home/ubuntu/workspace/testPinchas/assets/images/ImageBook/defBook.png
        //     _newBook.src = 'https://www.youtube.com/embed/' + youtubeID;
            $scope.busySubmittingImage = true;
            console.log("befor loading");
            $http.post('/books',_newBook).then(function(){
                console.log("added!!!! ");
                $scope.books.unshift(_newBook);
                $scope.busySubmittingImage = false;
                $scope.newBookTitle = '';
                $scope.newAuthorTitle = '';
                $scope.newImagSrc = '' ;
            });
    }
}]);