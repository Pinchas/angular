// /*global $*/
// $(function whenDomIsReady() {

//     $('.submit-video-form').submit(function(e) {
//         e.preventDefault();
//         var newVideo = {
//             title: $('.submit-video-form input[name="title"]').val(),
//             src: $('.submit-video-form input[name="src"]').val()
//         };
//         $('.submit-video-form input').val('');
//         $('.submit-video-form button').text('Submitting...');
//         $('.submit-video-form button').prop('disabled', true);
//         var parser = document.createElement('a');
//         parser.href = newVideo.src
//         var youtubeID = parser.search.substring(parser.search.indexOf("=") + 1,
//             parser.search.length);
//         newVideo.src = 'https://www.youtube.com/embed/' + youtubeID;
//         setTimeout(function() {
//             var newVideoHtml = '<li class="video">' +
//                 ' <h2>' + newVideo.title + '</h2>' +
//                 ' <iframe width="640" height="390" src="' + newVideo.src + '" frameborder="0" allowfullscreen></iframe>' +
//                 '</li>';
//             $('.the-list-of-videos').prepend(newVideoHtml);
//             $('.submit-video-form button').text('Submit Video');
//             $('.submit-video-form button').prop('disabled', false);
//         }, 3000);
//     });
// });

angular.module('brushfire_videosPage', []) 
.config(function($sceDelegateProvider) {
    $sceDelegateProvider.resourceUrlWhitelist([
        'self',
        '*://www.youtube.com/**'
    ]);
});

angular.module('brushfire_videosPage').controller('PageCtrl', [
    '$scope', '$timeout','$http',
    function($scope , $timeout , $http){
         $scope.videosLoading = true; 
         console.log("start. ");
         $http.get('/video').then(function(res){
             console.log('GET video ,data', res);
             $scope.videos = res.data;
             $scope.videosLoading = false; 
         });
        //  $timeout(function afterRetrievingVidoes (){
        //  var _videos = [
        //      {
        //       title: 'Nissan 370',
        //       src: 'https://www.youtube.com/v/V5JlkH7lCzE'
        //  },
        //  {
        //      title: 'Avicii - Live at Rock In Rio 2',
        //       src: 'https://www.youtube.com/v/m5idQFK0aAc'
        //  }];
        //  $scope.videosLoading = false;
        //  $scope.videos = _videos;
        //  console.log("end loading. ");
        //  }, 3000);
    
         $scope.submitNewVideo = function() {
            console.log("start submitNewVideo.....");
            if ($scope.busySubmittingVideo) {
                return;
            }       
            var _newVideo = {
                title: $scope.newVideoTitle, 
                artist: $scope.newArtistTitle,
                src: $scope.newVideoSrc, 
            };
            var parser = document.createElement('a');
            parser.href = _newVideo.src
            var youtubeID = parser.search.substring(parser.search.indexOf("=") + 1,  parser.search.length);
            _newVideo.src = 'https://www.youtube.com/embed/' + youtubeID;
            $scope.busySubmittingVideo = true;
            console.log("befor loading");
            // $timeout(function() {
            //     console.log("added ");
            //     $scope.videos.unshift(_newVideo);
            //     $scope.busySubmittingVideo = false;
            //     $scope.newVideoTitle = '';
            //     $scope.newVideoSrc = '';
            // }, 4000);
            // if($scope.newVideoTitle == "e.g. My video title" || $scope.newArtistTitle == "e.g. Artist"){
            // }
            
            $http.post('/video',_newVideo).then(function(){
                console.log("added!!!! ");
                $scope.videos.unshift(_newVideo);
                $scope.busySubmittingVideo = false;
                $scope.newVideoTitle = '';
                $scope.newArtistTitle = '';
                $scope.newVideoSrc = '';
                
            });
    }
}]);